#include "fftdi.h"

FTDI::FTDI( std::string req_serial, int speed, int product_id )
{
	try
	{
		ftdi_init_ok = false;

		if( ftdi_init( &ftdic ) < 0 )
			throw std::runtime_error( "ftdi_init failed" );

		ftdi_set_interface( &ftdic, INTERFACE_A );

		syslog( LOG_INFO, "Searching for FTDI dev serial [%s]", req_serial.c_str() );

		if( ftdi_usb_open_desc( &ftdic, 0x0403, product_id, NULL, req_serial.c_str() ) < 0 )
			throw std::runtime_error( "unable to open ftdi device" );

		ftdi_set_baudrate( &ftdic, speed );
		reply_vector.reserve( 255 );

		protoLen = 0;
		protoLenPos = 0;
		protoRepLen = 0;

		ftdi_init_ok = true;
		//unsigned int chipid;
		//printf( "ftdi_read_chipid: %d\n", ftdi_read_chipid( &ftdic, &chipid ) );
		//printf( "FTDI chipid: %X\n", chipid );
	}

	catch( const std::exception &ex )
	{
		syslog( LOG_INFO, "FTDI: %s", ex.what() );		
	}
}


FTDI::~FTDI()
{
	if( ftdi_init_ok )
		ftdi_usb_close( &ftdic );

	ftdi_deinit( &ftdic );
}

int FTDI::sendCmd( std::vector<unsigned char> cmd )
{
	reply_vector.clear();

	if( ftdi_write_data( &ftdic, cmd.data(), cmd.size() ) < 0 )
	{
		return 0;
	}

	int estimated_length = protoRepLen;

	for( int i=0; i<20000; i++ )
	{
		unsigned char tmp = 0;
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );

			if( estimated_length < 0 && reply_vector.size() > protoLenPos )
			{
				estimated_length = reply_vector[protoLenPos];
			}

			if( reply_vector.size() == estimated_length+protoLen )
				return 1;
		}
		else
		{
			usleep( 500 );
		}
	}

	return 0;
}

int FTDI::sendAckNak( std::vector<unsigned char> cmd )
{
	unsigned char tmp = 0;

	if( ftdi_write_data( &ftdic, cmd.data(), cmd.size() ) < 0 )
		return 0;

	for( int i=0; i<100; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			switch( tmp )
			{
				case 0xFB:
					return 0;

				case 0xFA:
					return 1;
			}
		}
		else
		{
			usleep( 500 );
		}
	}

	return 0;
}

int FTDI::writeBytes( std::vector<unsigned char> cmd )
{
	if( ftdi_write_data( &ftdic, cmd.data(), cmd.size() ) < 0 )
		return 0;

	return 1;
}

int FTDI::readBytes( unsigned int bytes )
{
	reply_vector.clear();

	for( int i=0; i<20000; i++ )
	{
		unsigned char tmp = 0;

		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );

			if( reply_vector.size() == bytes )
			{
				return 1;
			}
		}
		else
		{
			usleep( 500 );
		}
	}

	return 0;
}

int FTDI::readBytes()
{
	unsigned char tmp = 0;

	reply_vector.clear();

	int estimated_length = -1;

	for( int i=0; i<100; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );

			if( estimated_length < 0 && reply_vector.size() > protoLenPos )
			{
				estimated_length = reply_vector[protoLenPos];
			}

			if( reply_vector.size() == estimated_length+protoLen )
				return 1;
		}
		else
		{
			usleep( 1000 );
		}
	}

	return 0;
}

int FTDI::readLine()
{
	unsigned char tmp = 0;

	reply_vector.clear();

	for( int i=0; i<255; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );

			if( ( reply_vector[ reply_vector.size()-1 ] == 0x0A ) && ( reply_vector[ reply_vector.size()-2 ] == 0x0D ) )
				return 1;
		}
		else
		{
			usleep( 1000 );
		}
	}

	return 0;
}
