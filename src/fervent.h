#ifndef _FERVENT
#define _FERVENT

#include <atomic>
#include <queue>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <vector>

class semaphore
{
	private:
		std::mutex mutex_;
		std::condition_variable condition_;
		unsigned long count_;

	public:
		semaphore()
			: count_()
		{}

		void notify()
		{
			std::unique_lock<std::mutex> lock( mutex_ );
			++count_;
			condition_.notify_one();
		}

		void wait()
		{
			std::unique_lock<std::mutex> lock( mutex_ );

			while( !count_ )
				condition_.wait( lock );

			--count_;
		}
};

class Fervent
{
	public:
		Fervent() : atom_stop( false ) {}
		~Fervent()
		{
			for( uint32_t i=0; i<timer_threads.size(); i++ )
			{
				timer_threads[i]->join();
				delete( timer_threads[i] );
			}
		}

		void loop(); // run main loop
		void stop()  // stop main loop
		{
			atom_stop = true;
			sem.notify();
		}

		void addEvent( void ( *func )( void* ), void * );
		void addTimer( void ( *func )( void* ), void *,  unsigned long usec );

	private:
		struct task
		{
			void( *func )( void* );
			void *args;
		};

		std::vector<std::thread*> timer_threads;
		std::atomic<bool> atom_stop;
		std::queue<task> loop_queue;
		std::mutex loop_queue_lock;

		static void timedLoop( void *, void ( *event_ptr )( void* ), void *args, unsigned long usec );

		semaphore sem;
};

#endif