#ifndef ILZ2_H
#define ILZ2_H

#include "basereader.h"
#include "fftdi.h"
#include <sstream>

class IronLogic_Z2 : public BaseReader, public FTDI
{
	public:
		IronLogic_Z2( std::string serial ) : FTDI( serial, 9600, 0x1234 ) {}
		virtual ~IronLogic_Z2() {}
		
		// all virtual
		bool hardInit();
		BaseReader::uid_state poll();		
		bool beep() { return true; }
		bool setColorRed() { return true; }
		bool setColorGreen() { return true; }

	private:

};

#endif
