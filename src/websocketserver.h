#ifndef _WSS_H
#define _WSS_H

#include <string.h>
#include <thread>
#include <stdexcept>
#include <exception>
#include <queue>
#include <string>
#include <atomic>
#include <libwebsockets.h>

#include "fervent.h"

#define VERSION "0.1"

extern std::unique_ptr<Fervent> fev;
extern void websocket_report( void * );

using namespace std;

class WebSocketServer
{
	public:
		WebSocketServer( uint32_t p );
		virtual ~WebSocketServer()
		{
			lws_context_destroy( context );
		}

		void runThread();
		void stopThread();

		void addMessage( std::string msg )
		{			
			message_mutex.lock();
			messages.push( msg );
			message_mutex.unlock();
			lws_callback_on_writable_all_protocol( context, &protocols[0] );
		}

		std::string getMessage()
		{
			message_mutex.lock();
			std::string msg = messages.front();
			messages.pop();

			if( !messages.empty() )
				lws_callback_on_writable_all_protocol( context, &protocols[0] );

			message_mutex.unlock();

			return msg;
		}

		bool isConnected() { return is_connected; }

	private:
		uint32_t port;
		std::queue<std::string> messages;
		std::atomic<bool> atom_stop;
		std::atomic<bool> is_connected;

		std::mutex message_mutex;

		struct lws_context *context;
		struct lws_context_creation_info info;
		static struct lws_protocols protocols[];

		std::thread thr_handle;
		static void thr( WebSocketServer * );

		static int callback_dumb_increment( struct lws *wsi,
		                                    enum lws_callback_reasons reason,
		                                    void *user,
		                                    void *in,
		                                    size_t len );

		struct per_session_data__dumb_increment
		{
			int number;
		};
};

#endif
