#include "websocketserver.h"

WebSocketServer *wss_ptr = NULL;

int WebSocketServer::callback_dumb_increment( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
	std::string version;

//	struct per_session_data__dumb_increment *pss = ( struct per_session_data__dumb_increment * )user;

	switch( reason )
	{

		case LWS_CALLBACK_ESTABLISHED:
			if( wss_ptr->isConnected() == true )
			{
				return -1;
			}

			wss_ptr->is_connected = true;
//			logger.errorLog( "WSS: connection established\n" );


			version.assign( "{\"version\" : \"" );			
			version.append( VERSION );
			version.append( " \"} " );

			wss_ptr->addMessage( version );

			break;

		case LWS_CALLBACK_CLOSED:
			{
				wss_ptr->is_connected = false;
//				logger.errorLog( "WSS: connection closed\n" );
				break;
			}

		case LWS_CALLBACK_RECEIVE:   // the funny part
			{
				std::string *data = new std::string( ( char* )in, len );
				fev->addEvent( websocket_report, static_cast<void*>( data ) );
				break;
			}

		case LWS_CALLBACK_SERVER_WRITEABLE:
			{
				//printf( "WSS: ready to send\n" );

				if( !wss_ptr->messages.empty() )
				{
					std::string msg = wss_ptr->getMessage();

					unsigned char *buf = ( unsigned char* )malloc( LWS_SEND_BUFFER_PRE_PADDING + msg.length() + LWS_SEND_BUFFER_POST_PADDING );
					memcpy( buf + LWS_SEND_BUFFER_PRE_PADDING, msg.c_str(), msg.length() );
					lws_write( wsi, ( unsigned char* )buf+LWS_SEND_BUFFER_PRE_PADDING, msg.length(), LWS_WRITE_TEXT );
					free( buf );
				}

				break;
			}

		default:
			break;
	}

	return 0;
}


struct lws_protocols WebSocketServer::protocols[] =
{
	{
		"dumb", // protocol name - very important!
		callback_dumb_increment,   // callback
		sizeof( struct per_session_data__dumb_increment ),
		0
	},
	{
		NULL, NULL, 0, 0   /* End of list */
	}
};

void lwslog( int level, const char *line )
{
//	logger.errorLog( "LWS LOG: [%s]\n", line );
}

WebSocketServer::WebSocketServer( uint32_t p ) : port( p ), atom_stop( false ), is_connected( false )
{
	wss_ptr = this;
	//lws_set_log_level( LLL_DEBUG, lwslog );

	memset( &info, 0, sizeof info );

	info.port = p;
	info.gid = -1;
	info.uid = -1;
	info.protocols = protocols;
	info.max_http_header_pool = 1;
	info.options = 0;
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;

	context = lws_create_context( &info );

	if( context == NULL )
		throw runtime_error( "libwebsocket init failed" );
}

void WebSocketServer::runThread()
{
	try
	{
		thr_handle = std::thread( thr, this );
	}

	catch( const exception &ex )
	{
		printf( "WebSocketServer::runThread() - Error creating thread [error:%s]\n", ex.what() );
	}
}

void WebSocketServer::stopThread()
{
	if( thr_handle.joinable() )
	{
		atom_stop = true;
		thr_handle.join();
	}

	printf( "WebSocketServer stopped\n" );
}

void WebSocketServer::thr( WebSocketServer *srv )
{
	while( 1 )
	{
		lws_service( srv->context, 50 );

		if( srv->atom_stop == true )
			return;
	}
}
