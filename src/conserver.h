#ifndef _CONSERVER
#define _CONSERVER

#include "pkeyboard.h"
#include "sl500.h"
#include "ironlogic_z2.h"
#include "oldklava.h"
#include "newklava.h"
#include "websocketserver.h"
#include "sysinfo.h"

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#include <glob.h>

#include <libusb.h>
#include <string>
#include <iomanip>
#include <fstream>
#include <regex>

#define COMMAND_CONFIG "./scripts.json"

class proto_exception : public std::exception
{
    public:
        explicit proto_exception( int id, const char* message ): msg_( message ), id_( id ) {}
        explicit proto_exception( int id, const std::string &message ): msg_( message ), id_( id ) {}

        virtual ~proto_exception() throw() {}

        virtual const char *what() const throw()
        {
            return msg_.c_str();
        }

        virtual int getErrorCode() const throw()
        {
            return id_;
        }

    private:
        std::string msg_;
        int id_;
};

class ConServer
{
    public:
        ConServer();
        virtual ~ConServer();

        void nfcUID( BaseReader::UID * );
        void nfcRemoved( BaseReader::UID * );
        void newRequest( std::string );
        void keyboardStatus( PKeyboard::kbd_events *kevent );

    private:
        SysInfo sysinfo;
        Json::FastWriter writer;        
        WebSocketServer wssserver;
        std::unique_ptr<BaseReader> basereader;
        std::unique_ptr<PKeyboard> pkbd;

        struct dev_status
        {
            bool kbd_old;
            bool kbd_new;
            bool kbd_usb;
            bool sl500;
            bool z2;
        };

        dev_status status;

        void globFiles( const std::string &pattern, std::vector<std::string> &fileList );
        bool searchOldKlava();
        bool searchNewKlava();
        bool checkUSBKlava();
        bool search_SL500();
        bool search_IronLogic_Z2();

        bool parseJson( std::string json, Json::Value &root );
        std::string parseCommandConfig( std::string, Json::Value );
        void subStringReplace( std::string &, const std::string &, const std::string & );

        void sendMessage( std::string msg )
        {
            if( wssserver.isConnected() )
                wssserver.addMessage( msg );
        }

};


#endif
