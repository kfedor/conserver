#!/bin/sh

VERSION=`sed -n 's/Version: \([0-9\.\-]*\)/\1/p' dist/DEBIAN/control`
PKGNAME="conserver_${VERSION}_`arch`"

rm dist/usr/local/bin/conserver
cp ../src/conserver dist/usr/local/bin/
rsync -r --exclude=.git dist/ ${PKGNAME}
fakeroot dpkg-deb --build ${PKGNAME}
rm -rf ${PKGNAME}