#include "fervent.h"

void Fervent::loop()
{
	while( atom_stop == false )
	{
		sem.wait();		

		std::unique_lock<std::mutex> lk( loop_queue_lock );

		if( loop_queue.size() > 0 )
		{
			task t = loop_queue.front();

			void ( *func )( void* ) = t.func;
			void *args = t.args;
			( func )( args );

			loop_queue.pop();
		}
	}
}

void Fervent::addEvent( void ( *event_ptr )( void* ), void *args )
{
	std::unique_lock<std::mutex> lk( loop_queue_lock );

	task t;
	t.func = event_ptr;
	t.args = args;

	loop_queue.push( t );

	lk.unlock();
	sem.notify();
}

void Fervent::timedLoop( void *me, void ( *event_ptr )( void* ), void *args, unsigned long usec )
{
	Fervent *fev = static_cast<Fervent*>( me );

	while( fev->atom_stop == false )
	{
		fev->addEvent( event_ptr, args );
		std::this_thread::sleep_for( std::chrono::milliseconds( usec ) );
	}	
}

void Fervent::addTimer( void ( *func )( void* ), void *args,  unsigned long usec )
{
	std::thread *th = new std::thread( timedLoop, this, func, args, usec );
	timer_threads.push_back( th );	
}
