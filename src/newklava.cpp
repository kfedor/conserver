#include "newklava.h"

NewKlava::NewKlava()
{

}

NewKlava::~NewKlava()
{
    if( port.isOpen() )
        port.close();
}

bool NewKlava::init( std::string acm )
{
    if( port.open( acm.c_str(), B115200 ) )
        return true;

    return false;
}

short NewKlava::poll( unsigned short set_light )
{
    std::vector<unsigned char> reply;
    
    // get lights
    unsigned char cmd[] = { 0x55, 0xAA, cmds::get_lights, 0x00, 0x00, 0x00, 0x0A };    
    
    if( set_light )
    {
        cmd[2] = cmds::set_lights;
        cmd[3] = LOBYTE( set_light );
        cmd[4] = HIBYTE( set_light );        
    }
        
    port.write( cmd, 7 );

    unsigned int repLen = 0;

    while( 1 )
    {
        unsigned char buf = 0x00;
        port.read( &buf, 1 );
        reply.push_back( buf );

        if( reply.size() == 3 && !repLen )
        {
            if( reply[2] == 0x3A ) // button reply
            {
                repLen = 10;
            }
            else if( reply[2] == 0x3B )
            {
                repLen = 9;
            }
        }

        //if( reply.size()>6 && reply[reply.size()-1] == 0x0A )
        if( reply.size() == repLen )
            break;
    }

    //printHex( reply.data(), reply.size() );

    unsigned short keys_p = 0;
    unsigned short keys_r = 0;
    unsigned short lights = 0;

    if( reply[2] == 0x3A )
    {
        keys_p = ( unsigned short )( ( ( unsigned char )reply[3] ) << 8 | ( ( unsigned char )reply[4] ) );
        keys_r = ( unsigned short )( ( ( unsigned char )reply[5] ) << 8 | ( ( unsigned char )reply[6] ) );
    }
    else if( reply[2] == 0x3B ) // lights
    {
        lights  = ( unsigned short )( ( ( unsigned char )reply[3] ) << 8 | ( ( unsigned char )reply[4] ) );
    }

    if( keys_p )
    {
        printHex( reply.data(), reply.size() );
        printf( "keys pressed [%02X]\n", keys_p );        
    }

    if( keys_r )
    {
        printHex( reply.data(), reply.size() );
        printf( "keys released [%02X]\n", keys_r );        
    }

    if( lights )
        printf( "Lights [%02X]\n", lights );

    return keys_p;
}
