#include "crc.h"

void CRC::initCrcCcittTab( void )
{
	for( int i=0; i<256; i++ )
	{
		unsigned short crc = 0;
		unsigned short c = ( ( unsigned short ) i ) << 8;

		for( int j=0; j<8; j++ )
		{

			if( ( crc ^ c ) & 0x8000 )
				crc = ( crc << 1 ) ^ P_CCITT;
			else
				crc =   crc << 1;

			c = c << 1;
		}

		crc_tabccitt[i] = crc;
	}
}

unsigned short CRC::updateCrcCcitt( unsigned short crc, char c )
{
	unsigned short tmp=0x0, short_c=0x0;

	short_c  = 0x00ff & ( unsigned short ) c;

	tmp = ( crc >> 8 ) ^ short_c;
	crc = ( crc << 8 ) ^ crc_tabccitt[tmp];

	return crc;
}

unsigned short CRC::getCrcCcitt( char *data, int len )
{
	int i;
	unsigned short crc_ccitt_FFFF = 0xFFFF;

	for( i=0; i<len; i++ )
	{
		crc_ccitt_FFFF = updateCrcCcitt( crc_ccitt_FFFF, *data );
		data++;
	}

	return crc_ccitt_FFFF;
}
