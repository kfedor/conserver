#include "fserialport.h"

bool FSerialPort::isOpen()
{
    if( pdesc > 0 )
        return 1;

    return 0;
}
//0:0:8bd:0:0:0:0:0:0:e8:0:0:0:0:0:0:1:0:0:8:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
//35b819a7:0:3f9d4cfd:b5ab2ff4:34:ab:b5:0:0:32:0:38:8:0:0:dc:6d:98:b5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0

bool FSerialPort::open( const char *name, int baudRate )
{
    struct termios tc;

    memset( &tc, 0, sizeof( struct termios ) );

    //O_RDWR or O_NONBLOCK or O_NOCTTY moschip
    pdesc = ::open( name, O_RDWR ); //| O_NONBLOCK | O_NOCTTY );
    //pdesc = ::open( name, O_RDWR | O_NONBLOCK );

    if( pdesc < 0 )
        return false;

    //sleep( 2 );
    tcdrain( pdesc );
    tcflush( pdesc, TCIOFLUSH );

    cfsetspeed( &tc, baudRate );

    tc.c_cflag |= CS8 | CLOCAL | CREAD;
    tc.c_cflag &= ~CRTSCTS;

    //raw binary input / output
    tc.c_lflag &= ~( ICANON | ECHO | ECHOE | ISIG );
    tc.c_oflag &= ~OPOST;

    //no IXON/IXOFF flow control
    tc.c_iflag = 0;
    tc.c_iflag &= ~( IXON | IXOFF | IXANY );

    tc.c_cc[VMIN] = 0;       // threshold = 0 characters
    tc.c_cc[VTIME]  = 50;    // norm:50, timeout: 100 = 10 sec

    if( tcsetattr( pdesc, TCSAFLUSH, &tc ) < 0 )
    {
        ::close( pdesc );
        return false;
    }

    return true;
}

void FSerialPort::setDTR()
{
    int status = 0;
    status &= ~TIOCM_DTR;
    ioctl( pdesc, TIOCMSET, &status );
}

void FSerialPort::setRTS()
{
    int status = 0;
    status &= ~TIOCM_RTS;
    ioctl( pdesc, TIOCMSET, &status );
}

// PARENB added for Sankyo ICT
int FSerialPort::enableParityBit()
{
    struct termios tc;
    memset( &tc, 0, sizeof( struct termios ) );

    tcgetattr( pdesc, &tc );

    tc.c_cflag |= PARENB;

    if( tcsetattr( pdesc, TCSAFLUSH, &tc ) < 0 )
    {
        //logger.errorLog( "FSerialPort:: enabling parity bit failed\n" );
        return 0;
    }

    return 1;
}

int FSerialPort::set_7E1()
{
    struct termios options;
    memset( &options, 0, sizeof( struct termios ) );

    tcgetattr( pdesc, &options );

    //Even parity (7E1)
    options.c_cflag |= PARENB;
    options.c_cflag &= ~PARODD;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS7;

    if( tcsetattr( pdesc, TCSAFLUSH, &options ) < 0 )
    {
        //logger.errorLog( "FSerialPort:: enabling 7E1 failed\n" );
        return 0;
    }

    //logger.errorLog( "Set 7E1 OK (pdesc %d)\n", pdesc );

    return 1;
}

void FSerialPort::close()
{
    if( pdesc > 0 )
    {
        tcflush( pdesc, TCIOFLUSH );
        ::close( pdesc );
    }
}

int FSerialPort::write( unsigned char *data, int len )
{
    if( pdesc <= 0 )
        return 0;

//	logger.printHex( data, len );

    while( len > 0 )
    {
        int wr = ::write( pdesc, data, len );

        if( wr < 0 )
        {
            //logger.errorLog( "FSerialPort::write error\n" );
            return 0;
        }

        if( wr == 0 )
        {
            //logger.errorLog( "FSerialPort::write cannot send data\n" );
            return 0;
        }

        data += wr;
        len -= wr;
    }

    return 1;
}

int FSerialPort::read( unsigned char *buf, int bufsize )
{
    int ret;

    if( !pdesc )
        return 0;

    ret = ::read( pdesc, static_cast<unsigned char*>( buf ), bufsize );

    return ret;
}

int FSerialPort::selectRead( unsigned char *buf, int bufsize )
{
    int res;
    fd_set readfs;
    struct timeval tm;
    int r;

    FD_ZERO( &readfs );
    FD_SET( pdesc, &readfs );
    tm.tv_sec = 50; // sec
    tm.tv_usec = 0; // millisec

    do
    {
        r = select( pdesc + 1, &readfs, NULL, NULL, &tm );
    }
    while( r == 0 && !( tm.tv_sec == 0 && tm.tv_usec == 0 ) );

    if( r <= 0 )
    {
        //perror( "Select failed" );
        return 0;
    }

    res = ::read( pdesc, buf, bufsize );

    if( res < 0 )
    {
        //perror( "Read failed" );
        return 0;
    }

    return res;
}

int FSerialPort::sendRecv( unsigned char *command, int csize, unsigned char *buf, int bufsize )
{
    int res;
    fd_set readfs;
    struct timeval tm;
    int r;

    if( !write( command, csize ) )
    {
        printf("write err\n");
        return 0;
    }

    FD_ZERO( &readfs );
    FD_SET( pdesc, &readfs );
    tm.tv_sec = 5;
    tm.tv_usec = 0;

    do
    {
        r = select( pdesc + 1, &readfs, NULL, NULL, &tm );
    }
    while( r == 0 && !( tm.tv_sec == 0 && tm.tv_usec == 0 ) );

    if( r <= 0 )
    {
        printf("select err\n");
        return 0;
    }

    res = read( buf, bufsize );

    if( res < 0 )
    {
        printf("read err\n");
        return 0;
    }

    return res;
}
