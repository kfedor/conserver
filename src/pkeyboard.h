#ifndef _PKEYBOARD
#define _PKEYBOARD

#define LOBYTE(w) ((char)((short)(w) & 0xff))
#define HIBYTE(w) ((char)((short)(w) >> 8))

#include <thread>
#include <exception>
#include <atomic>
#include <deque>
#include <mutex>
#include <bitset>
#include <sys/time.h>
#include <syslog.h>
#include <unistd.h>
#include <string.h>

#include "fervent.h"

extern std::unique_ptr<Fervent> fev;
extern void keyboard_report( void *ptr );

class PKeyboard
{
    public:
        PKeyboard();
        virtual ~PKeyboard() {}

        void setLights( unsigned short li )
        {
            atom_lights = li;
        }

        unsigned short getKeys()
        {
            return atom_keys;
        }
        
        unsigned short getLights()
        {
            return atom_lights;
        }

        bool runThread();
        void stopThread();
        
        //virtual bool init() = 0;        

		struct kbd_events
		{
			unsigned short pressed;
			unsigned short released;
			uint64_t time;
		};

    protected:
        uint64_t getTimeMs();
        void printHex( unsigned char *cmd, int size );

    private:
        std::atomic<unsigned short> atom_lights;
        std::atomic<unsigned short> atom_keys;
        std::atomic<bool> atom_stop;

		kbd_events prev_event;
        std::thread handler;

        static void threadHandler( PKeyboard * );
        virtual short poll( unsigned short ) = 0;
        void checkKeys( unsigned short keys );
};

#endif
