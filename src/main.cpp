#include "fervent.h"
#include "conserver.h"

std::unique_ptr<ConServer> conserver;
std::unique_ptr<Fervent> fev;

static void signal_callback_handler( int signo )
{
    syslog( LOG_INFO, "Signal handler [%d]", signo );
    fev->stop();
}

void nfc_report_uid( void *ptr )
{
    conserver->nfcUID( static_cast<BaseReader::UID*>( ptr ) );
}

void nfc_report_removed( void *ptr )
{
    conserver->nfcRemoved( static_cast<BaseReader::UID*>( ptr ) );
}

void websocket_report( void *ptr )
{
    std::string *str = static_cast<std::string*>( ptr );
    conserver->newRequest( *str );
    delete( str );
}

void keyboard_report( void *ptr )
{    
    conserver->keyboardStatus( static_cast<PKeyboard::kbd_events*>( ptr ) );
}

int main()
{
    openlog( "conserver", LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER );
    setlogmask( LOG_UPTO( LOG_DEBUG ) );

    try
    {
        signal( SIGHUP, signal_callback_handler );
        signal( SIGINT, signal_callback_handler );
        signal( SIGTERM, signal_callback_handler );

        conserver = std::unique_ptr<ConServer>( new ConServer() );
        fev = std::unique_ptr<Fervent>( new Fervent() );

        fev->loop();
    }

    catch( std::exception &ex )
    {
        syslog( LOG_INFO, "Runtime error: %s\n", ex.what() );
    }

    syslog( LOG_INFO, "Exiting" );
    closelog();

    return 0;
}
