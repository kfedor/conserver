#include "sl500.h"

void printHex( std::vector<unsigned char> data )
{
	unsigned int sz = data.size();

	printf( "[%u]: ", sz );

	for( uint32_t i=0; i<sz; i++ )
		printf( "%02X ", data[i] );

	printf( "\n" );
}


bool SL500::openPort( std::string port )
{
	struct termios options;

	fd = open( port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY );

	if( fd == -1 )
	{
		//perror( "open_port: Unable to open /dev/ttyUSB0 - " );
		return false;
	}

	fcntl( fd, F_SETFL, 0 );

	tcgetattr( fd, &options );
	cfsetispeed( &options, B19200 );
	cfsetospeed( &options, B19200 );
	options.c_cflag |= ( CLOCAL | CREAD );
	options.c_cflag &= ~CSTOPB;
	cfmakeraw( &options );
    
    // test
    options.c_cc[VMIN] = 1;       // threshold = 0 characters
    options.c_cc[VTIME]  = 0;    // norm:50, timeout: 100 = 10 sec
    
	tcsetattr( fd, TCSANOW, &options );

	return true;
}

bool SL500::sendRecv( std::vector<unsigned char> cmd, BaseReader::UID &UID )
{
	unsigned char len = 0;
	unsigned char reply_char;
	std::vector<unsigned char> tmp;

	if( write( fd, cmd.data(), cmd.size() ) != (int32_t)cmd.size() )
		return false;

	for( uint32_t i=0; i<50; i++ )
	{
		int res = read( fd, &reply_char, 1 );

		if( res == 1 )
		{
			tmp.push_back( reply_char );

			if( tmp.size() == 3 )
				len = reply_char;

			if( tmp.size() == (uint32_t)len+4 )
				break;
		}
	}

	unsigned char crc = 0x00;

	for( uint32_t i=4; i<tmp.size()-1; i++ )
		crc ^= tmp[i];

	//printf( "CRC [%02X]\n", crc );

	if( crc != tmp[tmp.size()-1] )
		return false; // bad CRC

	if( tmp[8] != 00 )
		return false; // bad status

	std::copy( tmp.begin()+9, tmp.end()-1, back_inserter( UID ) );

	return true;
}

bool SL500::getModel()
{
	std::vector<unsigned char> cmd;
	std::vector<unsigned char> reply;

	cmd.push_back( 0xAA );
	cmd.push_back( 0xBB );
	cmd.push_back( 5 );
	cmd.push_back( 0x00 );
	cmd.push_back( 0x00 ); // dev_id
	cmd.push_back( 0x00 );
	cmd.push_back( 0x04 );
	cmd.push_back( 0x01 );

	unsigned char crc =  0x00;

	for( uint32_t i=4; i<cmd.size(); i++ )
		crc ^= cmd[i];

	cmd.push_back( crc );

	if( sendRecv( cmd, reply ) )
	{
//		printHex( reply );
		return true;
	}

	return false;
}

bool SL500::requestCard()
{
	std::vector<unsigned char> cmd;
	std::vector<unsigned char> reply;

	cmd.push_back( 0xAA );
	cmd.push_back( 0xBB );
	cmd.push_back( 6 );
	cmd.push_back( 0x00 );
	cmd.push_back( 0x00 ); // dev_id
	cmd.push_back( 0x00 );
	cmd.push_back( 0x01 );
	cmd.push_back( 0x02 );
	cmd.push_back( 0x52 ); // ALL

	unsigned char crc =  0x00;

	for( uint32_t i=4; i<cmd.size(); i++ )
		crc ^= cmd[i];

	cmd.push_back( crc );

	if( sendRecv( cmd, reply ) )
	{
//		printHex( reply );		
		return true;
	}

	return false;
}

bool SL500::getCardID( )
{
	std::vector<unsigned char> cmd;
	std::vector<unsigned char> reply;

	cmd.push_back( 0xAA );
	cmd.push_back( 0xBB );
	cmd.push_back( 5 );
	cmd.push_back( 0x00 );
	cmd.push_back( 0x00 ); // dev_id
	cmd.push_back( 0x00 );
	cmd.push_back( 0x02 );
	cmd.push_back( 0x02 );

	unsigned char crc =  0x00;

	for( uint32_t i=4; i<cmd.size(); i++ )
		crc ^= cmd[i];

	cmd.push_back( crc );

	if( sendRecv( cmd, reply ) )
	{
		new_uid = reply;
		//printHex( reply );
		return true;
	}

	return false;
}

bool SL500::beep()
{
	std::vector<unsigned char> cmd;
	std::vector<unsigned char> reply;

	cmd.push_back( 0xAA );
	cmd.push_back( 0xBB );
	cmd.push_back( 5 );
	cmd.push_back( 0x00 );
	cmd.push_back( 0x00 ); // dev_id
	cmd.push_back( 0x00 );
	cmd.push_back( 0x06 );
	cmd.push_back( 0x01 );

	unsigned char crc =  0x00;

	for( uint32_t i=4; i<cmd.size(); i++ )
		crc ^= cmd[i];

	cmd.push_back( crc );

	if( sendRecv( cmd, reply ) )
	{
//		printHex( reply );
		return true;
	}

	return false;
}

bool SL500::ledLight( led_color color )
{
	std::vector<unsigned char> cmd;
	std::vector<unsigned char> reply;

	cmd.push_back( 0xAA );
	cmd.push_back( 0xBB );
	cmd.push_back( 5+1 );
	cmd.push_back( 0x00 );
	cmd.push_back( 0x00 ); // dev_id
	cmd.push_back( 0x00 );
	cmd.push_back( 0x07 );
	cmd.push_back( 0x01 );
	cmd.push_back( color );

	unsigned char crc =  0x00;

	for( uint32_t i=4; i<cmd.size(); i++ )
		crc ^= cmd[i];

	cmd.push_back( crc );

	if( sendRecv( cmd, reply ) )
	{
//		printHex( reply );
		return true;
	}

	return false;
}

bool SL500::hardInit()
{    
	if( !beep() )
        return false;
        
	if( !getModel() )
        return false;

	return true;
}

BaseReader::uid_state SL500::poll()
{
	if( requestCard() && getCardID() )
	{
		return uid_state::state_new;
	}

	return uid_state::state_removed; 
}
