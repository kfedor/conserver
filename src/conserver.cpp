#include "conserver.h"

ConServer::ConServer() : wssserver( 8000 )
{
    memset( &status, 0, sizeof( dev_status ) );

    syslog( LOG_INFO, "Searching for NFC reader..." );

    if( checkUSBKlava() )
    {
        syslog( LOG_INFO, "USB Klava found" );
        status.kbd_usb = true;
    }

    syslog( LOG_INFO, "Searching for keyboard..." );

    if( searchOldKlava() || searchNewKlava() )
    {
        if( pkbd->runThread() )
            syslog( LOG_INFO, "Start keyboard thread OK" );
        else
            syslog( LOG_INFO, "Start keyboard thread failed" );
    }
    else
    {
        syslog( LOG_INFO, "No keyboards found" );
    }

    if( search_IronLogic_Z2() || search_SL500() )
    {
        if( basereader->runThread() )
            syslog( LOG_INFO, "Start reader thread OK" );
        else
            syslog( LOG_INFO, "Start reader thread failed" );
    }
    else
    {
        syslog( LOG_INFO, "No readers found" );
    }

    wssserver.runThread();
}

ConServer::~ConServer()
{
    if( basereader )
        basereader->stopThread();

    if( pkbd )
        pkbd->stopThread();

    wssserver.stopThread();
}

void ConServer::globFiles( const std::string &pattern, std::vector<std::string> &fileList )
{
    glob_t globbuf;

    glob( pattern.c_str(), GLOB_TILDE, NULL, &globbuf );

    for( unsigned int i = 0; i < globbuf.gl_pathc; ++i )
        fileList.push_back( globbuf.gl_pathv[i] );

    if( globbuf.gl_pathc > 0 )
        globfree( &globbuf );
}

bool ConServer::searchOldKlava()
{
    std::unique_ptr<OldKlava> oldKbd( new OldKlava( "A501H07X" ) );

    if( oldKbd->init() )
    {
        pkbd = std::move( oldKbd );
        syslog( LOG_INFO, "OldKlava found" );
        status.kbd_old = true;
        return true;
    }

    syslog( LOG_INFO, "OldKlava not found" );
    return false;
}

bool ConServer::searchNewKlava()
{
    std::vector<std::string> portList;
    globFiles( "/dev/ttyACM*", portList );

    std::unique_ptr<NewKlava> newKbd( new NewKlava() );

    for( auto i: portList )
    {
        if( newKbd->init( i ) )
        {
            pkbd = std::move( newKbd );
            syslog( LOG_INFO, "NewKlava found on %s", i.c_str() );
            status.kbd_new = true;
            return true;
        }
    }

    syslog( LOG_INFO, "NewKlava not found" );
    return false;
}

bool ConServer::checkUSBKlava()
{
    bool found = false;
    libusb_device **devs;

    if( libusb_init( NULL ) < 0 )
        return false;

    if( libusb_get_device_list( NULL, &devs ) < 0 )
        return false;

    for( auto i = 0; devs[i]; i++ )
    {
        struct libusb_device_descriptor desc;

        if( libusb_get_device_descriptor( devs[i], &desc ) < 0 )
            return false;

        for( auto a = 0; a < desc.bNumConfigurations; a++ )
        {
            struct libusb_config_descriptor *config;

            if( libusb_get_config_descriptor( devs[i], a, &config ) < 0 )
                return false;

            for( auto b = 0; b < config->bNumInterfaces; b++ )
            {
                for( auto c = 0; c < config->interface[b].num_altsetting; c++ )
                {
                    const struct libusb_interface_descriptor *i = &config->interface[b].altsetting[c];

                    //if( i->bInterfaceClass == 3 && i->bInterfaceSubClass == 1 && i->bInterfaceProtocol == 2 )
//                      printf( "USB MOUSE found\n" );

                    if( i->bInterfaceClass == 3 && i->bInterfaceSubClass == 1 && i->bInterfaceProtocol == 1 )
                    {
                        //printf( "USB KEYBOARD found\n" );
                        found = true;
                        break;
                    }
                }
            }

            libusb_free_config_descriptor( config );
        }
    }

    libusb_free_device_list( devs, 1 );
    libusb_exit( NULL );

    return found;

}


bool ConServer::search_SL500()
{
    std::vector<std::string> portList;
    globFiles( "/dev/ttyUSB*", portList );

    std::unique_ptr<SL500> sl500( new SL500() );

    for( auto i: portList )
    {
        if( sl500->openPort( i ) && sl500->hardInit() )
        {
            basereader = std::move( sl500 );
            syslog( LOG_INFO, "SL500 found on %s", i.c_str() );
            status.sl500 = true;
            return true;
        }
    }

    syslog( LOG_INFO, "SL500 not found" );

    return false;
}

bool ConServer::search_IronLogic_Z2()
{
    std::unique_ptr<IronLogic_Z2> z2( new IronLogic_Z2( "IL02ORNK" ) );

    if( z2->hardInit() )
    {
        basereader = std::move( z2 );
        syslog( LOG_INFO, "IronLogic Z2 found" );
        status.z2 = true;
        return true;
    }

    syslog( LOG_INFO, "IronLogic Z2 not found" );

    return false;
}

void ConServer::newRequest( std::string req )
{
    Json::Value root;
    Json::Value reply;

    try
    {
        if( !parseJson( req, root ) )
            throw proto_exception( 100, "JSON format error" );

        if( !root.isObject() )
            throw proto_exception( 100, "Not an object" );

        std::string command = root.get( "command", "" ).asString();

        if( command == "system" )
        {
            std::string arg = root.get( "script", "" ).asString();

            Json::Value params = root["params"];

            std::string exec_command = parseCommandConfig( arg, params );

            reply["command"] = "system";
            reply["code"] = system( exec_command.c_str() );
        }
        else if( command == "get_keys" )
        {
            if( pkbd == nullptr )
                throw proto_exception( 101, "Keyboard not connected" );

            reply["command"] = "get_keys";
            reply["keys"] = pkbd->getKeys();
        }
        else if( command == "get_lights" )
        {
            if( pkbd == nullptr )
                throw proto_exception( 101, "Keyboard not connected" );

            reply["command"] = "get_lights";
            reply["lights"] = pkbd->getKeys();
        }
        else if( command == "nfc_auth" )
        {
            if( basereader == nullptr )
                throw proto_exception( 102, "NFC Reader not connected" );

            BaseReader::UID *UID = new BaseReader::UID( basereader->getUID() );

            if( UID->size() > 0 )
                nfcUID( UID );
            else
                nfcRemoved( UID );
        }
        else if( command == "status" )
        {
            reply["command"] = "status";

            reply["data"]["motherboard"] = sysinfo.getBoard();
            reply["data"]["cpu"] = sysinfo.getCPU();
            reply["data"]["ram"] = sysinfo.getRAM();

            if( status.sl500 )
                reply["data"]["nfc"] = "sl500";
            else if( status.z2 )
                reply["data"]["nfc"] = "z2";

            if( status.kbd_usb )
            {
                reply["data"]["keyboard"].append( "usb" );
            }

            if( status.kbd_old )
            {
                reply["data"]["keyboard"].append( "old" );
            }
            else if( status.kbd_new )
            {
                reply["data"]["keyboard"].append( "new" );
            }

            if( !reply["data"].isObject() )
                reply["data"] = Json::objectValue;
        }
        else
        {
            throw proto_exception( 103, "Unknown command" );
        }
    }

    catch( proto_exception &ex )
    {
        reply["error_code"] = ex.getErrorCode();
        reply["error_string"] = ex.what();
    }

    sendMessage( writer.write( reply ) );
}

bool ConServer::parseJson( std::string json, Json::Value &root )
{
    Json::Reader reader;

    if( !reader.parse( json, root ) )
    {
        return false;
    }

    return true;
}

void ConServer::nfcUID( BaseReader::UID *temp )
{
    Json::Value value;

    std::ostringstream oss;

    for( auto i: *temp )
    {
        oss << hex << uppercase << std::setw( 2 ) << std::setfill( '0' ) << static_cast<int>( i );
    }

    value["command"] = "nfc_auth";
    value["status"] = "login";
    value["uid"] = oss.str();

    sendMessage( writer.write( value ) );
    delete( temp );
}

void ConServer::nfcRemoved( BaseReader::UID *temp )
{
    Json::Value value;

    std::ostringstream oss;

    for( auto i: *temp )
    {
        oss << hex << uppercase << std::setw( 2 ) << std::setfill( '0' ) << static_cast<int>( i );
    }

    value["command"] = "nfc_auth";
    value["status"] = "logout";
    value["uid"] = oss.str();

    sendMessage( writer.write( value ) );
    delete( temp );
}

void ConServer::keyboardStatus( PKeyboard::kbd_events *kevent )
{
    Json::Value value;

    value["command"] = "keyboard_event";
    value["keys_pressed"] = kevent->pressed;
    value["keys_released"] = kevent->released;
    value["time"] = std::to_string( kevent->time );

    sendMessage( writer.write( value ) );
    delete( kevent );
}

std::string ConServer::parseCommandConfig( std::string cmd, Json::Value jparams )
{
    Json::Reader reader;
    Json::Value root;
    std::ifstream in( COMMAND_CONFIG );

    std::string final_command;

    try
    {
        std::regex regParam( "\\$\\{.+?\\}" );

        if( cmd.length() == 0 )
            throw proto_exception( 104, "Bad command" );

        if( !in.is_open() )
            throw proto_exception( 104, "Error opening command config" );

        if( !reader.parse( in, root ) )
            throw proto_exception( 105, "Error parsing command config" );

        if( !root.isObject() )
            throw proto_exception( 106, "JSON Object check fail command config" );

        if( !jparams.isObject() )
            throw proto_exception( 106, "JSON Object check fail" );

        Json::Value commands = root["commands"];

        for( auto id : commands.getMemberNames() )
        {
            std::cout << "Cmds: " << id << std::endl;

            if( id == cmd )
            {
                std::string s_command = commands[id].get( "command", "" ).asString();
                std::string s_params = commands[id].get( "params", "" ).asString();
                bool fail_without_args = commands[id].get( "fail_without_arguments", false ).asBool();

                final_command.append( s_command );
                final_command.append( " " );

                std::sregex_iterator iter( s_params.begin(), s_params.end(), regParam );
                std::sregex_iterator end;

                //std::cout << "s params: " << s_params << std::endl;

                while( iter != end )
                {
                    //std::cout << "size: " << iter->size() << std::endl;

                    for( unsigned i = 0; i < iter->size(); ++i )
                    {
                        std::string tmpparam = ( *iter )[i]; // tag: ${data}
                        std::string nameparam = tmpparam.substr( 2, tmpparam.length()-3 ); // data

                        //std::cout << "tmpparam: " << tmpparam << std::endl;
                        //std::cout << "nameparam: " << nameparam << std::endl;

                        std::string jparam = jparams.get( nameparam, "" ).asString(); // value of data

                        // param found in json
                        if( jparam.length() > 0 )
                        {
                            subStringReplace( s_params, tmpparam, jparam );
                            std::cout << s_params << std::endl;
                        }
                        else
                        {
                            if( fail_without_args )
                                throw proto_exception( 201, "Missing required parameter in JSON" );
                            else
                                subStringReplace( s_params, tmpparam, "" );
                        }
                    }

                    ++iter;
                }

                // end success
                final_command.append( s_params );

                std::cout << "FINAL: " << final_command << std::endl;
                return final_command;

                /*
                { "command" : "system", "script" : "echo", "params" : { "text" : "vasily", "text2" : "ivan" } }
                */
            }
        }

        throw proto_exception( 200, "No such command in config" );
    }

    catch( std::runtime_error &ex )
    {
        std::cout << "EXCEPTION: " << ex.what() << std::endl;
        throw proto_exception( 202, "Regex error" );
    }
}

/*
std::string ConServer::subStringReplace( string const & in, string const & from, string const & to )
{
    return std::regex_replace( in, std::regex( from ), to );
}
*/
void ConServer::subStringReplace( std::string& str, const std::string& oldStr, const std::string& newStr )
{
    std::string::size_type pos = 0u;

    while( ( pos = str.find( oldStr, pos ) ) != std::string::npos )
    {
        str.replace( pos, oldStr.length(), newStr );
        pos += newStr.length();
    }
}
