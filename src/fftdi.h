#ifndef _FFTDI_H
#define _FFTDI_H

#include <ftdi.h>
#include <vector>
#include <stdexcept>
#include <string>
#include <iostream>

#include <syslog.h>

class FTDI
{
	public:
		FTDI( std::string, int speed, int product_id );
		virtual ~FTDI();

		int sendCmd( std::vector<unsigned char> );
		int sendAckNak( std::vector<unsigned char> );
		int readBytes();
		int readLine();
		int readBytes( unsigned int bytes );
		int writeBytes( std::vector<unsigned char> cmd );
		bool isInitOk() { return ftdi_init_ok; }
		std::vector<unsigned char> reply_vector;

	private:
		struct ftdi_context ftdic;

	protected:
		bool ftdi_init_ok;
		unsigned int protoLen;
		unsigned int protoLenPos;
		unsigned int protoRepLen;
};

#endif
