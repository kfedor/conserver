#include "basereader.h"

bool BaseReader::runThread()
{
	try
	{
		thr_handle = std::thread( thr, this );
	}

	catch( const std::exception &ex )
	{
		//logger.errorLog( "BaseReader::runThread() - Error creating thread [error:%d]\n", ex.what() );
		return false;
	}

	return true;
}

void BaseReader::thr( BaseReader *reader )
{		
	while( 1 )
	{
		if( reader->atom_exit == true )
		{
			printf( "BaseReader thread stopped\n" );
			return;
		}

		switch( reader->poll() )
		{
			case BaseReader::uid_state::state_removed:

				if( reader->old_uid.size() != 0 )
				{
					//printf( "card logout!\n" );

					auto tmp_ptr = new UID( reader->old_uid );
					fev->addEvent( nfc_report_removed, static_cast<void*>( tmp_ptr ) );

					reader->old_uid.clear();
					reader->setColorRed();
				}

				break;

			case BaseReader::uid_state::state_new:

				if( reader->new_uid != reader->old_uid )
				{
					// report old_uid logout if old_uid not empty
					if( reader->old_uid.size() != 0 )
					{
						//printf( "card replaced! logout old_uid\n" );

						auto tmp_ptr = new UID( reader->old_uid );
						fev->addEvent( nfc_report_removed, static_cast<void*>( tmp_ptr ) );
						reader->setColorRed();
					}

					//printf( "new card login - new_uid\n" );
					reader->old_uid = reader->new_uid;
					reader->beep();
					reader->setColorGreen();

					auto tmp_ptr = new UID( reader->old_uid );
					fev->addEvent( nfc_report_uid, static_cast<void*>( tmp_ptr ) );
				}

				std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
				break;

			case BaseReader::uid_state::state_nothing: // For future use
				break;
		} // end switch
	} // end while
}
