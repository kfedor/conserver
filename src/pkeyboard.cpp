#include "pkeyboard.h"

PKeyboard::PKeyboard()
{
    atom_stop = false;
    atom_lights = 0;
    atom_keys = 0;

    memset( &prev_event, 0, sizeof( kbd_events ) );
}

void PKeyboard::stopThread()
{
    if( handler.joinable() )
    {
        atom_stop = true;
        handler.join();
    }
}

bool PKeyboard::runThread()
{
    try
    {
        handler = std::thread( threadHandler, this );
    }

    catch( const std::exception& ex )
    {
        syslog( LOG_INFO, "Error creating thread" );
        return false;
    }

    return true;
}

void PKeyboard::threadHandler( PKeyboard* ptr )
{
    while( !ptr->atom_stop )
    {
        short keys = ptr->poll( ptr->atom_lights );

        if( keys >= 0 )
            ptr->checkKeys( keys );

        //usleep( 50000 ); // 50000 = 50ms
        usleep( 10000 ); // 50000 = 50ms
    }
}

void PKeyboard::checkKeys( unsigned short keys )
{
    if( prev_event.pressed == keys ) // no new keys
        return;

    syslog( LOG_INFO, "[%ld] Keys: %d", getTimeMs(), keys );

    // All pressed keys
    atom_keys |= keys;

    kbd_events *tmp = new kbd_events;
    tmp->pressed = keys;
    tmp->released = ( keys ^ prev_event.pressed ) & prev_event.pressed;
    tmp->time = getTimeMs();

    memcpy( &prev_event, tmp, sizeof( kbd_events ) );

    fev->addEvent( keyboard_report, static_cast<void*>( tmp ) );
}


uint64_t PKeyboard::getTimeMs()
{
    struct timeval te;

    gettimeofday( &te, NULL ); // get current time
    uint64_t milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds

    return milliseconds;
}

void PKeyboard::printHex( unsigned char *cmd, int size )
{
    printf( "[%d]: ", size );

    for( int i=0; i<size; i++ )
        printf( "%02X ", cmd[i] );

    printf( "\n" );
}
