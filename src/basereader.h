#ifndef BASEREADER_H
#define BASEREADER_H

#include <vector>
#include <thread>
#include <exception>

#include "fervent.h"

extern std::unique_ptr<Fervent> fev;
extern void nfc_report_uid( void *ptr );
extern void nfc_report_removed( void *ptr );

class BaseReader
{
	public:
		BaseReader() : atom_exit( false ) {}
		virtual ~BaseReader() {}

		typedef std::vector<unsigned char> UID; // array 7 ?
		bool runThread();

		void stopThread()
		{
			if( thr_handle.joinable() )
			{
				atom_exit = true;
				thr_handle.join();				
			}
		}

		UID getUID() { return old_uid; }		

	protected:
		UID new_uid;
		UID old_uid;

		enum uid_state
		{
			state_new = 0x01,
			state_removed,
			state_nothing
		};

	private:		
		std::thread thr_handle;
		std::atomic<bool> atom_exit;

		static void thr( BaseReader * );
		virtual bool hardInit() = 0;
		virtual uid_state poll() = 0;
		virtual bool beep() = 0;
		virtual bool setColorRed() = 0;
		virtual bool setColorGreen() = 0;
};

#endif
