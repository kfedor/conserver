#ifndef _SPORT
#define _SPORT

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>

#include <termios.h>
#include <sys/ioctl.h>
#include <string.h>


class FSerialPort
{
	public:
		FSerialPort() : pdesc( 0 ) {}

		int set_7E1();
		int enableParityBit();
		bool isOpen();

		bool open( const char *, int );
		int write( unsigned char *data, int len );
		int read( unsigned char *buf, int bufsize );
		void close();
		int selectRead( unsigned char *buf, int bufsize );
		int sendRecv( unsigned char *command, int csize, unsigned char *buf, int bufsize );
		int getFD() { return pdesc; };
		void flush() { tcflush( pdesc, TCSANOW ); };
		void setDTR();
		void setRTS();

	private:
		int pdesc;
};

#endif
