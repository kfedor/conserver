#ifndef _OLDKLAVA
#define _OLDKLAVA

#include "pkeyboard.h"
#include "crc.h"
#include "fftdi.h"

class OldKlava : public PKeyboard, public FTDI
{
    public:
        OldKlava( std::string serial ) : FTDI( serial, 115200, 0x6001 ) {}

        bool init()
        {
            if( !isInitOk() )
                return false;

            return true;
        }

    protected:
        short poll( unsigned short );

    private:        
        CRC crc;

        short ftdiLights( unsigned short );
        short gameKeysToKeys( unsigned short );
};

#endif
