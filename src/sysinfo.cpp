#include "sysinfo.h"

SysInfo::SysInfo()
{
    getBoardInfo();
    getCPUInfo();
    getRAMInfo();
}

void SysInfo::getBoardInfo()
{
    std::string buffer;
    std::ifstream t;

    t.open( BOARD_VENDOR );
    std::getline( t, buffer );
    board.append( buffer );
    t.close();

    board.append( " " );

    t.open( BOARD_NAME );
    std::getline( t, buffer );
    board.append( buffer );
    t.close();
}

void SysInfo::getCPUInfo()
{
    std::string buffer;
    std::ifstream t;

    t.open( CPU_INFO );

    while( t )
    {
        std::getline( t, buffer );

        if( buffer.compare( 0, 10, "model name" ) == 0 )
        {
            cpu.assign( buffer.substr( 13, buffer.length()-13 ) );
            break;
        }
    }

    t.close();
}

void SysInfo::getRAMInfo()
{
    struct sysinfo sys_info;

    if( sysinfo( &sys_info ) == -1 )
        return;

    unsigned long total_ram = ( ( uint64_t ) sys_info.totalram * sys_info.mem_unit )/1024;
    ram = std::to_string( total_ram );
}
