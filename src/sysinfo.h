#include <fstream>
#include <string>
#include <iostream>

#include <sys/sysinfo.h>

#define BOARD_NAME "/sys/devices/virtual/dmi/id/subsystem/id/board_name"
#define BOARD_VENDOR "/sys/devices/virtual/dmi/id/subsystem/id/board_vendor"
#define CPU_INFO "/proc/cpuinfo"

class SysInfo
{
    public:
        SysInfo();
        std::string getBoard() { return board; }
        std::string getCPU() { return cpu; }
        std::string getRAM() { return ram; }

    private:
        std::string board;
        std::string cpu;
        std::string ram;
        void getBoardInfo();
        void getCPUInfo();
        void getRAMInfo();
};
