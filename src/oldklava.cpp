#include "oldklava.h"

short OldKlava::poll( unsigned short light )
{    
    std::vector<unsigned char> cmd = { 0x55, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    
    unsigned short ftdi_light = ftdiLights( light );

	cmd[2] = LOBYTE( ftdi_light );
	cmd[3] = HIBYTE( ftdi_light );

	unsigned short new_crc = crc.getCrcCcitt( (char*)cmd.data()+2, cmd.size()-4 );

	cmd[6] = LOBYTE( new_crc );
	cmd[7] = HIBYTE( new_crc );
    
    if( !writeBytes( cmd ) )
    {
        syslog( LOG_INFO, "Error writing to FTDI" );
		return -1;
    }
    
    if( !readBytes( 12 ) )
    {
        syslog( LOG_INFO, "Error reading from FTDI" );
		return -1;
    }
    
    if( reply_vector[0] != 0x55 && reply_vector[1] != 0xAA )
		return -1;
    
    //syslog( LOG_INFO, "KEYS OK" );
    unsigned short keys = ( unsigned short )( ( ( unsigned char )reply_vector[9] ) << 8 | ( ( unsigned char )reply_vector[8] ) );

	return gameKeysToKeys( keys );    
}


short OldKlava::ftdiLights( unsigned short in )
{
	std::bitset<12> bin = in;
	std::bitset<12> bout = 0;

	if( bin.test( 0 ) )  //
		bout.set( 10 );   // 1 pin = 1024

	if( bin.test( 1 ) )  //
		bout.set( 4 );  // 2 pin = 16

	if( bin.test( 2 ) )  //
		bout.set( 5 );  // 3 pin = 32

	if( bin.test( 3 ) )  //
		bout.set( 8 );   // 4 pin = 256

//----
	if( bin.test( 4 ) ) //
		bout.set( 3 );   // 5 pin = 8

	if( bin.test( 5 ) ) //
		bout.set( 2 );   // 6 pin = 4

	if( bin.test( 6 ) ) //
		bout.set( 1 );   // 7 pin = 2

	if( bin.test( 7 ) ) //
		bout.set( 0 );   // 8 pin = 1

//----
	if( bin.test( 8 ) )  //
		bout.set( 7 );   // 9 pin = 128

	if( bin.test( 9 ) )  //
		bout.set( 6 );   // 10 pin = 64

	if( bin.test( 10 ) ) //
		bout.set( 9 );   // 11 pin = 512

	if( bin.test( 11 ) ) //
		bout.set( 11 );	 // 12 = 2048

	return ( unsigned short )bout.to_ulong();
}

short OldKlava::gameKeysToKeys( unsigned short in )
{
	std::bitset<16> bin = in;
	std::bitset<12> bout = 0;

	if( bin.test( 0 ) )  // 1 pin = 1
		bout.set( 9 );  // 1 pin = 512

	if( bin.test( 1 ) )  // 2 pin = 2
		bout.set( 2 );   // 2 pin = 4

	if( bin.test( 2 ) )  // 3 pin = 4
		bout.set( 1 );   // 3 pin = 2

	if( bin.test( 3 ) )  // 4 pin = 8
		bout.set( 7 );   // 4 pin = 128

//----
	if( bin.test( 15 ) ) // 5 pin = 32768
		bout.set( 6 );   // 5 pin = 64

	if( bin.test( 14 ) ) // 6 pin = 16384
		bout.set( 5 );   // 6 pin = 32

	if( bin.test( 13 ) ) // 7 pin = 8192
		bout.set( 4 );   // 7 pin = 16

	if( bin.test( 12 ) ) // 8 pin = 4096
		bout.set( 3 );   // 8 pin = 8

//----
	if( bin.test( 8 ) )  // 9 pin = 256
		bout.set( 10 );   // 9 pin = 1024

	if( bin.test( 9 ) )  // 10 pin = 512
		bout.set( 0 );   // 10 pin = 1

	if( bin.test( 10 ) ) // 11 pin = 1024
		bout.set( 11 );  // 11 pin = 2048

	if( bin.test( 11 ) ) // 12 pin = 2048
		bout.set( 8 );	 // 12 = 256

	return ( unsigned short )bout.to_ulong();
}
