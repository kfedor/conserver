#ifndef _NEWKLAVA
#define _NEWKLAVA

#include "pkeyboard.h"
#include "fserialport.h"
#include <vector>


class NewKlava : public PKeyboard
{
    public:
        NewKlava();
        ~NewKlava();
        bool init( std::string );
        
        enum cmds
        {
            reset_button_timer = 0x30,
            get_lights = 0x31,
            set_lights = 0x32,
            rgb_1 = 0x33,
            rgb_2 = 0x34,
            rgb_3 = 0x35,
            rgb_4 = 0x36,
            rgb_5 = 0x37,
            demo = 0x38
        };

    private:
        FSerialPort port;
        short poll( unsigned short light );
};

#endif
