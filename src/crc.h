#ifndef _CRC
#define _CRC

#define P_CCITT  0x1021

class CRC
{
	public:
		CRC() { initCrcCcittTab(); }
		unsigned short getCrcCcitt( char *data, int len );

	private:
		unsigned short crc_tabccitt[256];

		unsigned short updateCrcCcitt( unsigned short crc, char c );
		void initCrcCcittTab( void );
};

#endif
