#include "ironlogic_z2.h"

bool IronLogic_Z2::hardInit()
{
	if( !isInitOk() )
		return false;
		
	std::vector<unsigned char> wr = { 0x69 };
	writeBytes( wr );

	for( uint32_t i=0; i<6; i++ )
	{
		if( !readLine() )
			return false;

		std::string out( reply_vector.begin(), reply_vector.end() );
		printf( "%s", out.c_str() );
	}

	return true;
}

BaseReader::uid_state IronLogic_Z2::poll()
{
	if( readLine() )
	{
		std::string out( reply_vector.begin(), reply_vector.end() );
		//printf( ">>%s", out.c_str() );

		if( out.compare( 0, 7, "No card" ) == 0 )
		{
			return uid_state::state_removed; // Card removed
		}
		else if( out.compare( 0, 6, "Mifare" ) == 0 )
		{
			new_uid.clear();

			std::string uid = out.substr( out.find( "[" )+1, 14 );

			//printf( "UID [%s]\n", uid.c_str() );

			for( uint32_t i = 8; i < uid.size(); i += 2 )
			{
				std::istringstream strm( uid.substr( i, 2 ) );
				unsigned int x;
				strm >> std::hex >> x;
				new_uid.push_back( static_cast<unsigned char>( x ) );
			}
			
			for( uint32_t i = 0; i < 8; i += 2 )
			{
				std::istringstream strm( uid.substr( i, 2 ) );
				unsigned int x;
				strm >> std::hex >> x;
				new_uid.push_back( static_cast<unsigned char>( x ) );
			}

			return uid_state::state_new; // NEW UID read
		}
	}

	return uid_state::state_nothing; // nothing changed
}
