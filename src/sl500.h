#ifndef SL500_H
#define SL500_H

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string>
#include <vector>

#include "basereader.h"

class SL500 : public BaseReader
{
	public:
		enum led_color
		{
		    led_off = 0x00,
		    led_red,
		    led_green
		};

		bool hardInit();
		BaseReader::uid_state poll();

		bool openPort( std::string port );
		bool getModel();
		bool requestCard();
		bool getCardID();
		bool beep();
		bool setColorRed() { return ledLight( led_color::led_red ); }
		bool setColorGreen() { return ledLight( led_color::led_green ); }
		
	private:
		int fd;
		bool sendRecv( std::vector<unsigned char> cmd, BaseReader::UID &UID );
		bool ledLight( led_color );
};

#endif
